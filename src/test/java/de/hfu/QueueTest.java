package de.hfu;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.hfu.unit.Queue;

public class QueueTest {

    private static Queue queue;

    @BeforeEach
    public void testInit() {
        queue = new Queue(3);
    }

    @Test
    public void testDequeueCorrectOrderWithoutReplacing() {
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);

        assertTrue(queue.dequeue() == 1);
        assertTrue(queue.dequeue() == 2);
        assertTrue(queue.dequeue() == 3);
    }

    @Test
    public void testDequeueCorrectOrderWithReplacingMultiple() {
        for (int i = 1; i <= 10; i++)
            queue.enqueue(i);

        assertTrue(queue.dequeue() == 1);
        assertTrue(queue.dequeue() == 2);
        assertTrue(queue.dequeue() == 10);
    }

    @Test
    public void testDequeueCorrectOrderWithDequeueFirstAndReplacingMultiple() {
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);

        queue.dequeue();

        queue.enqueue(4);
        queue.enqueue(5);

        assertTrue(queue.dequeue() == 2);
        assertTrue(queue.dequeue() == 3);
        assertTrue(queue.dequeue() == 5);
    }

    @Test
    public void testIllegalConstructorArgumentThrowsException() {
        int sizes[] = { 0, -1, -2, Integer.MIN_VALUE };

        for (int i : sizes) {
            var exception = assertThrows(IllegalArgumentException.class, () -> new Queue(i));
            assertTrue(exception.getMessage().equals("Queue Arraylength " + i));
        }
    }

    @Test
    public void testDequeueEmptyQueueThrowsException() {
        var exception = assertThrows(IllegalStateException.class, () -> queue.dequeue());
        assertTrue(exception.getMessage().equals("dequeue on empty queue"));
    }

}
