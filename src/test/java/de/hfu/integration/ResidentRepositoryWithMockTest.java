package de.hfu.integration;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import org.easymock.EasyMock;
import org.junit.jupiter.api.*;

import de.hfu.integration.domain.Resident;
import de.hfu.integration.repository.ResidentRepository;
import de.hfu.integration.service.BaseResidentService;
import de.hfu.integration.service.ResidentServiceException;

import java.util.ArrayList;
import java.util.Calendar;

public class ResidentRepositoryWithMockTest {

    private static ResidentRepository repositoryMock;
    private static BaseResidentService residentService;

    @BeforeEach
    public void init() {
        var residents = new ArrayList<Resident>();
        var calendar = Calendar.getInstance();
        calendar.set(1985, 1, 1);

        residents.add(new Resident("Alice", "White", "Bear Valley", "Raccon City", calendar.getTime()));
        residents.add(new Resident("Bob", "Blue", "Cat Street", "Furtwangen", calendar.getTime()));

        calendar.set(1986, 5, 20);
        residents.add(new Resident("Chris", "Red", "Dog Street", "Furtwangen", calendar.getTime()));
        calendar.set(1983, 6, 13);
        residents.add(new Resident("Dave", "Green", "Hamster Street", "Los Santos", calendar.getTime()));
        calendar.set(1982, 2, 12);
        residents.add(new Resident("Eugene", "Blue", "Hamster Street", "Los Santos", calendar.getTime()));

        repositoryMock = EasyMock.createMock(ResidentRepository.class);
        EasyMock.expect(repositoryMock.getResidents()).andReturn(residents);
        EasyMock.replay(repositoryMock);

        residentService = new BaseResidentService();
        residentService.setResidentRepository(repositoryMock);
    }

    @Test
    public void testGetFilteredResidentsWhoseLastNameStartsWithB() {
        Resident filter = new Resident();
        filter.setFamilyName("B*");

        var results = residentService.getFilteredResidentsList(filter);
        assertThat(results.size(), equalTo(2));
        EasyMock.verify(repositoryMock);
    }

    @Test
    public void testGetFilteredResidentsWhoseStreetContainsStreet() {
        Resident filter = new Resident();
        filter.setStreet("*street*");

        var results = residentService.getFilteredResidentsList(filter);
        assertThat(results.size(), equalTo(4));
        EasyMock.verify(repositoryMock);
    }

    @Test
    public void testGetFilteredResidentsWithSameDOB() {
        Resident filter = new Resident();
        var calendar = Calendar.getInstance();
        calendar.set(1985, 1, 1);
        filter.setDateOfBirth(calendar.getTime());

        var results = residentService.getFilteredResidentsList(filter);
        assertThat(results.size(), equalTo(2));
        EasyMock.verify(repositoryMock);
    }

    @Test
    public void testGetUniqueResidentFromSpecificCityAndStreet() {
        Resident search = new Resident();
        search.setCity("Furtwangen");
        search.setStreet("Cat Street");

        try {
            var result = residentService.getUniqueResident(search);
            assertThat(result.getGivenName(), equalTo("Bob"));
            assertThat(result.getFamilyName(), equalTo("Blue"));
        } catch (Exception e) {
            fail("Existing Resident not found");
        }

        EasyMock.verify(repositoryMock);
    }

    @Test
    public void testGetUniqueResidentNotExistent() {
        Resident search = new Resident();
        search.setCity("Donaueschingen");

        var exception = assertThrows(ResidentServiceException.class, () -> {
            residentService.getUniqueResident(search);
        });

        assertThat(exception.getMessage(), equalTo("Suchanfrage lieferte kein eindeutiges Ergebnis!"));
        EasyMock.verify(repositoryMock);
    }

    @Test
    public void testGetUniqueResidentWithWildcardThrowsException() {
        Resident search = new Resident();
        search.setFamilyName("B*");

        var exception = assertThrows(ResidentServiceException.class, () -> {
            residentService.getUniqueResident(search);
        });

        assertThat(exception.getMessage(), equalTo("Wildcards (*) sind nicht erlaubt!"));
    }

}
