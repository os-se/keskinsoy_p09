package de.hfu.integration;

import org.junit.jupiter.api.*;

import de.hfu.integration.domain.Resident;
import de.hfu.integration.repository.ResidentRepositoryStub;
import de.hfu.integration.service.BaseResidentService;
import de.hfu.integration.service.ResidentServiceException;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.Calendar;

public class ResidentRepositoryTest {

    private static BaseResidentService residentService;

    @BeforeAll
    public static void initAll() {
        var repo = new ResidentRepositoryStub();

        var calendar = Calendar.getInstance();
        calendar.set(1985, 1, 1);

        repo.addResident(new Resident("Alice", "White", "Bear Valley", "Raccon City", calendar.getTime()));
        repo.addResident(new Resident("Bob", "Blue", "Cat Street", "Furtwangen", calendar.getTime()));

        calendar.set(1986, 5, 20);
        repo.addResident(new Resident("Chris", "Red", "Dog Street", "Furtwangen", calendar.getTime()));
        calendar.set(1983, 6, 13);
        repo.addResident(new Resident("Dave", "Green", "Hamster Street", "Los Santos", calendar.getTime()));
        calendar.set(1982, 2, 12);
        repo.addResident(new Resident("Eugene", "Blue", "Hamster Street", "Los Santos", calendar.getTime()));

        residentService = new BaseResidentService();
        residentService.setResidentRepository(repo);
    }

    @BeforeEach
    public void init() {
    }

    @Test
    public void testGetFilteredResidentsByLastNameAndWildcard() {
        Resident filter = new Resident();
        filter.setFamilyName("B*");

        var results = residentService.getFilteredResidentsList(filter);
        assertTrue(results.size() == 2);
    }

    @Test
    public void testGetFilteredResidentsByFamilyNameAndWildcard() {
        Resident filter = new Resident();
        filter.setFamilyName("*re*");

        var results = residentService.getFilteredResidentsList(filter);
        assertTrue(results.size() == 2);
    }

    @Test
    public void testGetFilteredResidentsByStreetAndWildcard() {
        Resident filter = new Resident();
        filter.setStreet("*Street");

        var results = residentService.getFilteredResidentsList(filter);
        assertTrue(results.size() == 4);
    }

    @Test
    public void testGetFilteredResidentsByCityAndWildcard() {
        Resident filter = new Resident();
        filter.setCity("Furtwangen");

        var results = residentService.getFilteredResidentsList(filter);
        assertTrue(results.size() == 5);
    }

    @Test
    public void testGetFilteredResidentsByDateOfBirth() {
        Resident filter = new Resident();
        var calendar = Calendar.getInstance();
        calendar.set(1985, 1, 1);
        filter.setDateOfBirth(calendar.getTime());

        var results = residentService.getFilteredResidentsList(filter);
        assertTrue(results.size() == 2);
    }

    @Test
    public void testGetUniqueResidentFromSpecificCityAndStreet() {
        Resident search = new Resident();
        search.setCity("Furtwangen");
        search.setStreet("Cat Street");

        try {
            var result = residentService.getUniqueResident(search);
            assertTrue(result.getGivenName() == "Bob");
            assertTrue(result.getFamilyName() == "Blue");
        } catch (Exception e) {
            fail("Existing Resident not found");
        }

    }

    @Test
    public void testGetUniqueResidentNotExistent() {
        Resident search = new Resident();
        search.setGivenName("Peter");

        var exception = assertThrows(ResidentServiceException.class, () -> {
            residentService.getUniqueResident(search);
        });

        assertTrue(exception.getMessage().equals("Suchanfrage lieferte kein eindeutiges Ergebnis!"));
    }

    @Test
    public void testGetUniqueResidentReturnsMultiple() {
        Resident search = new Resident();
        search.setFamilyName("Blue");

        var exception = assertThrows(ResidentServiceException.class, () -> {
            residentService.getUniqueResident(search);
        });

        assertTrue(exception.getMessage().equals("Suchanfrage lieferte kein eindeutiges Ergebnis!"));
    }

    @Test
    public void testGetUniqueResidentWithWildcardThrowsException() {
        Resident search = new Resident();
        search.setFamilyName("B*");

        var exception = assertThrows(ResidentServiceException.class, () -> {
            residentService.getUniqueResident(search);
        });

        assertTrue(exception.getMessage().equals("Wildcards (*) sind nicht erlaubt!"));
    }

}