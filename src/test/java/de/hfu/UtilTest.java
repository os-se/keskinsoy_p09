package de.hfu;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import de.hfu.unit.Util;

public class UtilTest {

    @Test
    public void testErstesHalbjahrTrue() {
        assertTrue(Util.istErstesHalbjahr(1));
        assertTrue(Util.istErstesHalbjahr(2));
        assertTrue(Util.istErstesHalbjahr(3));
        assertTrue(Util.istErstesHalbjahr(4));
        assertTrue(Util.istErstesHalbjahr(5));
        assertTrue(Util.istErstesHalbjahr(6));
    }

    @Test
    public void testErstesHalbjahrFalse() {
        assertFalse(Util.istErstesHalbjahr(7));
        assertFalse(Util.istErstesHalbjahr(8));
        assertFalse(Util.istErstesHalbjahr(9));
        assertFalse(Util.istErstesHalbjahr(10));
        assertFalse(Util.istErstesHalbjahr(11));
        assertFalse(Util.istErstesHalbjahr(12));
    }

    @Test
    public void testIllegalArguments() {
        int values[] = { 0, 13, 14, Integer.MAX_VALUE, -1, -2, Integer.MIN_VALUE };
        for (int i : values) {
            assertThrows(IllegalArgumentException.class, () -> Util.istErstesHalbjahr(i));
        }
    }

}
