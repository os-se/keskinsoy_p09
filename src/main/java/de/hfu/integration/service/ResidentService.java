package de.hfu.integration.service;

import java.util.List;

import de.hfu.integration.domain.Resident;

/**
 * @author Stefan Betermieux
 */
public interface ResidentService {

  Resident getUniqueResident(Resident filterResident) throws ResidentServiceException;

  // returns a filtered List of Resident objects
  List<Resident> getFilteredResidentsList(Resident filterResident);

}